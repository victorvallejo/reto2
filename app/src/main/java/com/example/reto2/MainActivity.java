package com.example.reto2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ListView listAnimals;

    String[][] animals = {
            {"Guacamaya Bandera", "El guacamayo rojo, guacamayo macao o guacamaya bandera es una especie de ave perteneciente a la familia de los psitácidos..."},
            {"Taruca", "La taruca (Hippocamelus antisensis) es un venado que se siente de lo más cómodo andando por las escarpadas faldas de los montes andinos..."},
            {"Tamarino león dorado", "También conocido como tití león dorado o tití leoncito es un primate platirrino que vive en el oriente de Brasil..."},
            {"Manatí del Amazonas", "El manatí del Amazonas es una de las tres especies de manatíes que se encuentran en Sudamérica y África...."},
            {"Tortuga de las Galápagos", "Esta gigante tortuga que es un ícono a nivel mundial vive en las famosas islas Galápagos..."},
            {"Albatros de las Galápagos", "Esta ave es el único miembro de la familia Diomedeidae que vive en los trópicos y sólo se le encuentra en las islas Galápagos..."},
            {"Vaquita Marina", "Los cetáceos suelen ser muy grandes y pesados. No lo es la vaquita marina (Phocoena sinus), que apenas llega a los 50 kg y a la que solo parecen gustarle las aguas del Golfo de California o Mar de Cortés..."}
    };

    int[] imgAnimals = {R.drawable.guacamaya, R.drawable.taruca, R.drawable.titidorado, R.drawable.manatiamazonas, R.drawable.tortugaa, R.drawable.albatrosgalapagos, R.drawable.vaquitamarinaa};
    int[] soundAnimals = {R.raw.guacamaya, R.raw.taruca, R.raw.titi, R.raw.manati, R.raw.tortuga, R.raw.albatros, R.raw.manati};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listAnimals = (ListView) findViewById(R.id.list_view_animals);

        listAnimals.setAdapter(new MyAdapter(this, animals, imgAnimals));

        listAnimals.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent viewDescription = new Intent(view.getContext(), DescripcionAnimal.class);
                viewDescription.putExtra("IMG", imgAnimals[position]);
                viewDescription.putExtra("TITLE", animals[position][0]);
                viewDescription.putExtra("DES", animals[position]);
                viewDescription.putExtra("SOUND", soundAnimals[position]);
                startActivity(viewDescription);
            }
        });
    }

}