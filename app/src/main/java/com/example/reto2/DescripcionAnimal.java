package com.example.reto2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class DescripcionAnimal extends AppCompatActivity {

    MediaPlayer media;
    int positionSound;
    int positionDescription;

    String animals[] = {
            "En su famosa novela, El amor en los tiempos del cólera, el premio Nobel colombiano, Gabriel García Márquez, narra que uno de los personajes principales, el doctor Juvenal Urbino, tenía un loro que hablaba español y francés, enseñado por su dueño, que había estudiado Medicina en Francia. El loro era una atracción turística por su bilingüismo y en una ocasión en la que el Presidente de la República visitó Cartagena de Indias, principal escenario de la novela, Urbino quiso pavonearse con su loro intelectual frente al Jefe del Estado. Quedó avergonzado porque el animal se negó a pronunciar palabra. La Guacamaya Bandera (Ara macao) no habla francés, al menos en vida silvestre, pero imita muy bien las voces que escucha a su alrededor. Está en fuerte peligro de extinción porque es inmisericordemente cazada para vender sus bellas plumas.",
            "La taruca (Hippocamelus antisensis) es un venado que se siente de lo más cómodo andando por las escarpadas faldas de los montes andinos. Vive en varios de los países que comparten la Cordillera de los Andes, pero es en Argentina donde está más amenazado. Los cazadores son sus principales enemigos, seguido de las modificaciones en sus espacios naturales de vida.",
            "También conocido como tití león dorado o tití leoncito es un primate platirrino que vive en el oriente de Brasil. Se estima que son cerca de mil ejemplares los que quedan. A pesar de los programas para apoyar su reproducción, estos tiernos animales continúan siendo parte de los animales apunto de extinguirse.",
            "El manatí del Amazonas es una de las tres especies de manatíes que se encuentran en Sudamérica y África. La población de este hermoso animal ha disminuido un 30 % en los últimos años y se cree que desaparecen por la pérdida de su territorio y el envenenamiento de mercurio causado por la minería ilegal.",
            "Esta gigante tortuga que es un ícono a nivel mundial vive en las famosas islas Galápagos, sin embargo, este paraíso natural se ha visto amenazado desde hace siglos, cuando los marineros y pescadores redujeron su número de manera extraordinaria. Hoy, con la inclusión de animales invasores como las cabras en el hábitat de las tortugas, éstas se han quedado sin comida.",
            "Esta ave es el único miembro de la familia Diomedeidae que vive en los trópicos y sólo se le encuentra en las islas Galápagos. Con alas que le dan una longitud de 2.4 metros y llegando a vivir cerca de 80 años, esta magnífica ave puede estar dando sus últimas vueltas al sol si no hacemos algo para evitar que el cambio climático con ella.",
            "Los cetáceos suelen ser muy grandes y pesados. No lo es la vaquita marina (Phocoena sinus), que apenas llega a los 50 kg y a la que solo parecen gustarle las aguas del Golfo de California o Mar de Cortés. Se estima que solo quedan menos de cien ejemplares. Su principal amenaza es la pesca indiscriminada en la búsqueda de otras especies comerciales."
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descripcion_animal);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ImageView image = (ImageView) findViewById(R.id.animal_image_des);
        TextView name = (TextView) findViewById(R.id.animal_name_des);
        TextView description = (TextView) findViewById(R.id.animal_description_des);
        ImageButton sound = (ImageButton) findViewById(R.id.button_sound);

        final Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if (bundle != null){
            image.setImageResource(bundle.getInt("IMG"));
            name.setText(bundle.getString("TITLE"));
            positionDescription = bundle.getInt("DES");
            description.setText(animals[positionDescription]);
            positionSound = bundle.getInt("SOUND");
            sound.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    media = MediaPlayer.create(getApplicationContext(),positionSound);
                    media.start();
                }
            });
        }
    }
}