package com.example.reto2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MyAdapter extends BaseAdapter {

    private static LayoutInflater inflater = null;
    Context context;
    String[][] animals;
    int[] imageAnimals;

    public MyAdapter(Context context, String[][] animals, int[] imageAnimals) {
        this.context = context;
        this.animals = animals;
        this.imageAnimals = imageAnimals;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return imageAnimals.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final View VISTA = inflater.inflate(R.layout.elemento_lista, null);

        TextView name = (TextView) VISTA.findViewById(R.id.animal_name);
        TextView description = (TextView) VISTA.findViewById(R.id.animal_description);
        ImageView image = (ImageView) VISTA.findViewById(R.id.animal_image);

        image.setImageResource(imageAnimals[i]);
        name.setText(animals[i][0]);
        description.setText(animals[i][1]);

        return VISTA;
    }
}
